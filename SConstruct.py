import os
import sys
sys.path.append(os.path.join(os.getcwd(),'vendor'))

import excons
import excons.config
import excons.tools.maya as maya
import excons.tools.gl as gl

maya.SetupMscver()
env = excons.MakeBaseEnv()

version = (1, 0, 0)
versionstr = "%d.%d.%d" % version
platname = {"win32": "windows", "darwin": "osx"}.get(sys.platform, "linux")
outprefix = "plug-ins/%s/%s/%s" % (maya.Version(nice=True), platname, excons.arch_dir)

outdir = excons.OutputBaseDirectory()

gen = excons.config.AddGenerator(env, "dm2sc", {"DM2SC_VERSION": "[%d, %d, %d]" % version,
                                              "DM2SC_MAJMIN_VERSION": "%d.%d" % (version[0], version[1]),
                                              "DM2SC_VERSION_MAJOR": "%d" % version[0],
                                              "DM2SC_VERSION_MINOR": "%d" % version[1],
                                              "DM2SC_VERSION_PATCH": "%d" % version[2]})
# mrsinit = gen("app/scripts/mrs/__init__.py", "app/scripts/mrs/__init__.py.in")
# mrsversion = gen("app/scripts/mrs/version.py", "app/scripts/mrs/version.py.in")
dm2scMod = gen("dm2sc.mod", "dm2sc.mod.in")
# mrspy = filter(lambda x: not os.path.basename(x).startswith("__init__.py"), excons.glob("scripts/superbox/*"))
# NoClean(mrsinit + superboxMod)

defines = []
if sys.platform == "win32":
    defines.append("NOMINMAX")


def maya_math_nodes_setup(env):
    env.Append(CPPDEFINES=[('NODE_NAME_PREFIX', '\"\\\"math_\\\"\"')])
    env.Append(CCFLAGS=["-Os"])
    env.Append(CPPFLAGS=" -DPROJECT_VERSION=\"\\\"1.4.0\\\"\"")


# def CVWrapSetup(env):
#     if sys.platform == "win32":
#         env.Append(CCFLAGS=["/arch:AVX"])
#     else:
#         env.Append(CCFLAGS=["-mavx"])


# qjason = ["vendor/QJsonModel/qjsonmodel.py"]
# qtpy = excons.glob("vendor/qtpy/qtpy/*")

targets = [
    {
        "name": "core",
        "type": "install",
        "desc": "core modules",
        "install": {
            "scripts": excons.glob("scripts/*"),
            "scenes": excons.glob("scenes/*"),
            "": dm2scMod + excons.glob("README.md") + excons.glob("install.mel"),
        }
    },
    {
        "name": "deltaMushToSkinCluster",
        "type": "dynamicmodule",
        "desc": "delta mush to skin cluster conver",
        "prefix": outprefix,
        "bldprefix": maya.Version(),
        "ext": maya.PluginExt(),
        "defs": defines,
        "incdirs": ["src"],
        "srcs": excons.glob("src/*.cpp"),
        "custom": [maya.Require]
    }
]

excons.AddHelpTargets(dm2sc="maya (core, deltaMushToSkinCluster)")

td = excons.DeclareTargets(env, targets)

env.Alias("dm2sc", [
        td["core"], 
        td["deltaMushToSkinCluster"]
    ]
)

td["python"] = filter(lambda x: os.path.splitext(str(x))[1] != ".mel", Glob(outdir + "scripts/*"))
td["scripts"] = Glob(outdir + "scripts/*.mel")

pluginsdir = "/plug-ins/%s/%s" % (maya.Version(nice=True), excons.EcosystemPlatform())

ecodirs = {
    "deltaMushToSkinCluster": pluginsdir,
    # "mrsSolvers": pluginsdir,
    # "cvwrap": pluginsdir,
    # "grim_IK": pluginsdir,
    # "maya-math-nodes": pluginsdir,
    # "weightDriver": pluginsdir,
    # "python": "/python",
    "scripts": "/scripts"
}

excons.EcosystemDist(env, "dm2sc.env", ecodirs, version=versionstr, targets=td)

Default(["dm2sc"])
