//
//  pluginMain.cpp
//  deltaMushToSkinCluster
//
//  Created by Godfrey Huang on 10/9/15.
//
//

#include <maya/MFnPlugin.h>
#include "deltaMushToSkinCluster.h"

MStatus initializePlugin( MObject obj )
{
    MStatus   status;
    MFnPlugin plugin( obj, "Godfrey Huang", "1.0", "Any");
    
    status = plugin.registerCommand( "deltaMushToSkinCluster",
                                    deltaMushToSkinCluster::creator,
                                    deltaMushToSkinCluster::newSyntax);
    if (!status) {
        status.perror("registerCommand");
        return status;
    }
    
    return status;
}

MStatus uninitializePlugin( MObject obj)
{
    MStatus	  status;
    MFnPlugin plugin( obj );
    
    status = plugin.deregisterCommand( "deltaMushToSkinCluster" );
    if (!status) {
        status.perror("deregisterCommand");
        return status;
    }
    
    return status;
}