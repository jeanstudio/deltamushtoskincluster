#include "deltaMushToSkinCluster.h"
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MDagPathArray.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnMesh.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MFnTransform.h>
#include <maya/MGlobal.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MItMeshVertex.h>
#include <maya/MObject.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MSelectionList.h>
#include <maya/MTimer.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MBoundingBox.h>

#define kSourceFlag         "-s"
#define kSourceFlagLong     "-source"
#define kTargetFlag         "-t"
#define kTargetFlagLong     "-target"

using namespace std;

deltaMushToSkinCluster::deltaMushToSkinCluster()
{
}

deltaMushToSkinCluster::~deltaMushToSkinCluster(){}

void* deltaMushToSkinCluster::creator()
{
    return new deltaMushToSkinCluster;
}

MSyntax deltaMushToSkinCluster::newSyntax()
{
    MSyntax syntax;
    syntax.addFlag(kSourceFlag,kSourceFlagLong,MSyntax::kString);
    syntax.addFlag(kTargetFlag,kTargetFlagLong,MSyntax::kString);
    return syntax;
}

MStatus deltaMushToSkinCluster::parseArgs(const MArgList &args, MString &source, MString &target)
{
    MArgDatabase argData(syntax(),args);
    source = 0.0;
    target = 0.0;
    if (argData.isFlagSet(kSourceFlag))
    {
        argData.getFlagArgument(kSourceFlag, 0, source);
    }
    
    if (argData.isFlagSet(kSourceFlagLong))
    {
        argData.getFlagArgument(kSourceFlagLong, 0, source);
    }
    
    if (argData.isFlagSet(kTargetFlag))
    {
        argData.getFlagArgument(kTargetFlag, 0, target);
    }
    
    if (argData.isFlagSet(kTargetFlagLong))
    {
        argData.getFlagArgument(kTargetFlagLong, 0, target);
    }
    
    return MS::kSuccess;
}

MObject deltaMushToSkinCluster::findSkinCluster(MDagPath& dagPath)
{
    MStatus status;
    MObject skinCluster;
    MObject geomNode = dagPath.node();
    MItDependencyGraph dgIt(geomNode, MFn::kSkinClusterFilter, MItDependencyGraph::kUpstream);
    if (!dgIt.isDone()) {
        skinCluster = dgIt.currentItem();
    }
    return skinCluster;
}

MBoundingBox deltaMushToSkinCluster::getBoundingBox(MDagPath& dagPath)
{
    MBoundingBox boundingBox;
    MFnMesh meshFn(dagPath);
    MPointArray pointArray;
    MPoint point;
    MPoint newMinPoint;
    MPoint newMaxPoint;
    
    meshFn.getPoints( pointArray, MSpace::kWorld);
    
    for (unsigned i=0; i<pointArray.length(); i++) {
        point = pointArray[i];
        boundingBox.expand(point);
    }
    
    newMinPoint = boundingBox.min();
    newMaxPoint = boundingBox.max();
    boundingBox.expand(newMinPoint);
    boundingBox.expand(newMaxPoint);
    
    return boundingBox;
}

MStatus deltaMushToSkinCluster::doIt( const MArgList& args )
{
    // record start time
    MTimer timer;
    timer.beginTimer();
    
    MStatus status;
    status = parseArgs(args, source, target);
    
    MSelectionList sList;
    MDagPath sourceDP,targetDP;
    MObject sourceComponent,targetComponent;
    MFnMesh sourceMesh;
    sList.add(source);
    sList.add(target);
    sList.getDagPath(0, sourceDP, sourceComponent);
    sList.getDagPath(1,targetDP,targetComponent);
    sourceMesh.setObject(sourceDP);
    
    // get mesh's boundingBox info
    MBoundingBox boundingBox = getBoundingBox(sourceDP);
//    double offset = (boundingBox.width()+boundingBox.height()+boundingBox.depth())/10.0;
//    double factor = offset/10000.0;
    double offset = 1.0;
    double factor = 0.01;
    
    MItMeshVertex sourceMeshVertexIt(sourceDP,sourceComponent, &status);
    unsigned int numSourceMeshVertex = sourceMeshVertexIt.count();
    if (sourceComponent.isNull()) sourceDP.extendToShape();
    
    MObject sourceSkinCluster = findSkinCluster(sourceDP);
    MObject targetSkinCluster = findSkinCluster(targetDP);
    MFnSkinCluster sourceSkinClusterFn(sourceSkinCluster,&status);
    MFnSkinCluster targetSkinClusterFn(targetSkinCluster,&status);
    
    MDagPathArray sourceInfPathArray;
    sourceSkinClusterFn.influenceObjects(sourceInfPathArray,&status);
    unsigned int numSourceInfObjects = sourceInfPathArray.length();
    MVector infJointTransformVectorOld,infJointTransformVectorNew;
    MVectorArray infChildJointTransformVectorArray;
    MFnTransform infJointTransformFn;
    MPointArray sourceMeshVertexPointArrayOld;
    for (; !sourceMeshVertexIt.isDone(); sourceMeshVertexIt.next())
    {
        MPoint pt = sourceMeshVertexIt.position(MSpace::kWorld);
        sourceMeshVertexPointArrayOld.append(pt);
    }
    sourceMeshVertexIt.reset();
    
    MFnDependencyNode sourceSkinClusterDepFn(targetSkinCluster);
    MPlug pWeightList = sourceSkinClusterDepFn.findPlug("weightList");
    
    MDagPath sourceInfPath;
    MObject chidObject;
    MFnDagNode childDagNode;
    MDagPath childDagPath;
    MFnTransform childInfJointTransformFn;
    unsigned int chindCount;
    for (unsigned int i=0; i<numSourceInfObjects; i++)
    {
        sourceInfPath = sourceInfPathArray[i];
        chindCount = sourceInfPath.childCount();
        
        infChildJointTransformVectorArray.clear();
        // store childs vector
        for (unsigned int j=0; j<chindCount; j++) {
            chidObject = sourceInfPath.child(j);
            childDagNode.setObject(chidObject);
            childDagNode.getPath(childDagPath);
            childInfJointTransformFn.setObject(childDagPath);
            infChildJointTransformVectorArray.append(childInfJointTransformFn.getTranslation(MSpace::kWorld));
        }
        
        // offset the influence joint
        infJointTransformFn.setObject(sourceInfPath);
        infJointTransformVectorOld = infJointTransformFn.getTranslation(MSpace::kWorld);
        infJointTransformVectorNew = infJointTransformVectorOld;
        infJointTransformVectorNew.operator+=(MVector(0,offset,0));
        infJointTransformFn.setTranslation(infJointTransformVectorNew, MSpace::kWorld);
//        MGlobal::executeCommand(MString("refresh;\n"));
//        usleep(puaseTime);
        
        // offset the child influence object
        for (unsigned k=0; k<chindCount; k++) {
            chidObject = sourceInfPath.child(k);
            childDagNode.setObject(chidObject);
            childDagNode.getPath(childDagPath);
            childInfJointTransformFn.setObject(childDagPath);
            childInfJointTransformFn.setTranslation(infChildJointTransformVectorArray[k], MSpace::kWorld);
        }
//        MGlobal::executeCommand(MString("refresh;\n"));

        // store offset affter point
        MPointArray sourceMeshVertexPointArrayNew;
        for (; !sourceMeshVertexIt.isDone(); sourceMeshVertexIt.next())
        {
            MPoint pt = sourceMeshVertexIt.position(MSpace::kWorld);
            sourceMeshVertexPointArrayNew.append(pt);
        }
        sourceMeshVertexIt.reset();

        for (unsigned int l=0; l<numSourceMeshVertex; l++)
        {
            MPlug pWeightListElem = pWeightList.elementByLogicalIndex(l);
            MPlug pWeight = pWeightListElem.child(0);
            
            MPoint oldPt = sourceMeshVertexPointArrayOld[l];
            MPoint newPt = sourceMeshVertexPointArrayNew[l];
            double dist = oldPt.distanceTo(newPt);
            
            MPlug pWeightElement = pWeight.elementByLogicalIndex(i);
            
            if (dist>factor)
            {
                double weight = dist/offset;
                pWeightElement.setValue(weight);
            }else{
                pWeightElement.setValue(0.0);
            }
        }
        // re-offset the influence joint
        infJointTransformFn.setTranslation(infJointTransformVectorOld, MSpace::kWorld);
        for (unsigned m=0; m<chindCount; m++) {
            chidObject = sourceInfPath.child(m);
            childDagNode.setObject(chidObject);
            childDagNode.getPath(childDagPath);
            childInfJointTransformFn.setObject(childDagPath);
            childInfJointTransformFn.setTranslation(infChildJointTransformVectorArray[m], MSpace::kWorld);
        }
//        MGlobal::executeCommand(MString("refresh;\n"));
    }
    
    MString skinNormalizeStr = "select -r \"" + target + "\";\n";
    skinNormalizeStr += "skinPercent -normalize true \"" + targetSkinClusterFn.name() +"\";";
    MGlobal::executeCommand(skinNormalizeStr);

    timer.endTimer();
    MGlobal::displayInfo(MString("Calculating Time: ") + timer.elapsedTime() + " Seconds");
    return MS::kSuccess;
}
